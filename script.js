class Calculator {
    constructor(savedDisplayText, currentDisplayText) {
        this.savedDisplayText = savedDisplayText;
        this.currentDisplayText = currentDisplayText;
        this.clear();
    }

    clear() {
        this.currentDisplay = "";
        this.savedDisplay = "";
        this.operator = undefined;
    }

    delete() {
        if(this.currentDisplay === ""){return}
        this.currentDisplay = this.currentDisplay.slice(0, -1);
    }

    addNumber(number) {
        if (number === '.' && this.currentDisplay.includes('.')) return
        this.currentDisplay = this.currentDisplay.toString() + number.toString();
    }

    selectOperator(operator) {
        if (this.currentDisplay === "") return;
        if (this.currentDisplay !== "") {
            this.compute();
        }
        this.operator = operator;
        this.savedDisplay = this.currentDisplay;
        this.currentDisplay = "";
    }

    compute() {
        let computation;
        const saved = parseFloat(this.savedDisplay);
        const current = parseFloat(this.currentDisplay);
        if (isNaN(saved) || isNaN(current)) return;
        switch (this.operator) {
            case "+": computation = saved + current;
                break;
            case "-": computation = saved - current;
                break;
            case "*": computation = saved * current;
                break;
            case "/": computation = saved / current;
                break;
            default: return;
        }
        this.currentDisplay = computation;
        this.operator = undefined;
        this.savedDisplay = "";
    }

    updateDisplay() {
        this.currentDisplayText.innerText = this.currentDisplay;
        this.savedDisplayText.innerText = this.savedDisplay;



    }


}


const numberButton = document.querySelectorAll('[data-number]');
const operatorButton = document.querySelectorAll('[data-operator]');
const equalsButton = document.querySelector('[data-equals]');
const deleteButton = document.querySelector('[data-del]');
const clearButton = document.querySelector('[data-clear]');
const savedDisplayText = document.querySelector('[data-saved-display-text]');
const currentDisplayText = document.querySelector('[data-current-display-text]');

const calculator = new Calculator(savedDisplayText,
    currentDisplayText);

numberButton.forEach(button => {
    button.addEventListener("click", () => {
        calculator.addNumber(button.innerText);
        calculator.updateDisplay();
    })
})

clearButton.addEventListener("click", () => {
    calculator.clear();
    calculator.updateDisplay();
})

deleteButton.addEventListener("click", () => {
    calculator.delete();
    calculator.updateDisplay();
})

operatorButton.forEach(button => {
    button.addEventListener("click", () => {
        calculator.selectOperator(button.innerText);
        calculator.updateDisplay();
    })
})

equalsButton.addEventListener("click", () => {
    calculator.compute();
    calculator.updateDisplay();
})